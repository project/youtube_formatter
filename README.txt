Introduction
-------------
The youtube formatter module provides a simple textfield display 
formatter for youtube links.


Avalible formatters
-------------------
youtube preview
This formatter will display the preview image, provided by youtube, 
in a selected image_style

youtube video
Embed the video via iframe. The size and some options 
are avalible to configure the video

youtube preview with colorbox integration
Displays the preview image. Onclick it will open the video inside 
a colorbox overlay, if colorbox is avalible


colorbox integration
--------------------
To use the Colorbox integration, you have to install and enable 
the colorbox module and enable the "Colorbox load" function at the settingspage.
Instructions

Install the module on your Drupalsetup as usal. Youtube Formatter will add 
the three possible Formatters to the standard drupal text field! Simply add 
a new text field to your entity and choose the formatter you need.
The url the formatter expects is built like http://youtu.be/videoID and 
can be found below every youtube video, through the share button.
